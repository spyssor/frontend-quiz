import { fetchData } from './utils/FetchData';
import { renderHeader } from './views/renderHeader';
import { createPerson } from './utils/CreatePerson';
import { renderAboutMe } from './views/renderAboutMe';
import { renderEducations } from './views/renderEducations';
const URL = 'http://localhost:3000/person';

fetchData(URL)
  .then(result => {
    console.log(result);
    const { name, age, description, educations } = createPerson(result);
    renderHeader(name, age);
    renderAboutMe(description);
    renderEducations(educations);
  })
  .catch(error => {
    logger.error(error);
  });
