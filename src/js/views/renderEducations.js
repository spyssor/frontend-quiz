import $ from 'jquery';
export function renderEducations(educations) {
  educations.forEach(education =>
    $('ul').append(`<li>
                          <h3 id="year">${education.year}</h3>
                          <div id="detail">
                            <h3 class="title">${education.title}</h3>
                            <p class="education-description">${
                              education.description
                            }</p>
                          </div>
                        </li>`)
  );
}
