export function fetchData(URL) {
  return fetch(URL).then(response => response.json());
}
