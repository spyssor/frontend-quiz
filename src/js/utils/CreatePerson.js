import { Person } from '../entity/Person';

export function createPerson(json) {
  const { name, age, description, educations } = json;
  return new Person(name, age, description, educations);
}
